# vuejs-instance-de-vue

VueJS - Instance de vue

## Détail

-   Créer une instance de Vue
    -   Vue : https://fr.vuejs.org/v2/guide/instance.html#Creer-une-instance-de-Vue
    -   Cours supplémentaire : https://www.tutorialspoint.com/vuejs/vuejs_instances.htm
    -   Propriétés d'instance : https://fr.vuejs.org/v2/api/#Proprietes-d%E2%80%99instance
    -   Exercice : https://gitlab.com/webausore/vuejs/instance-de-vue/creation
-   Données et méthodes
    -   Vue : https://fr.vuejs.org/v2/guide/instance.html#Donnees-et-methodes
    -   Méthodes et données d'instance : https://fr.vuejs.org/v2/api/#Methodes-et-donnees-d%E2%80%99instance
    -   Exercice : https://gitlab.com/webausore/vuejs/instance-de-vue/donnees-et-methodes
-   Hooks de cycle de vie d'une instance + Diagramme
    -   Vue : https://fr.vuejs.org/v2/guide/instance.html#Hooks-de-cycle-de-vie-d%E2%80%99une-instance / https://fr.vuejs.org/v2/guide/instance.html#Diagramme-du-cycle-de-vie
    -   Cours vidéo : https://vueschool.io/lessons/understanding-the-vuejs-lifecycle-hooks?friend=vuejs
    -   Exercice : https://gitlab.com/webausore/vuejs/instance-de-vue/hooks
